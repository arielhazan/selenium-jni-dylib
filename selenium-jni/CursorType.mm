//
//  CursorType.mm
//  selenium-jni
//
//  Created by ariel on 02/09/2020.
//  Copyright © 2020 ariel. All rights reserved.
//
#import "CursorType.h"
#import <stdio.h>

/*
NSPoint arrow = [[NSCursor arrowCursor] hotSpot];
NSPoint iBeam = [[NSCursor IBeamCursor] hotSpot];
NSPoint resizeUpDown = [[NSCursor resizeUpDownCursor] hotSpot];
NSPoint contextualMenu = [[NSCursor contextualMenuCursor] hotSpot];
NSPoint pointingHand = [[NSCursor pointingHandCursor] hotSpot];
NSPoint closedHand = [[NSCursor closedHandCursor] hotSpot];
NSPoint crosshair = [[NSCursor crosshairCursor] hotSpot];
NSPoint disappearingItem = [[NSCursor disappearingItemCursor] hotSpot];
NSPoint dragCopy = [[NSCursor dragCopyCursor] hotSpot];
NSPoint dragLink = [[NSCursor dragLinkCursor] hotSpot];
NSPoint openHand = [[NSCursor openHandCursor] hotSpot];
NSPoint operationNotAllowed = [[NSCursor operationNotAllowedCursor] hotSpot];
NSPoint resizeDown = [[NSCursor resizeDownCursor] hotSpot];
NSPoint resizeLeft = [[NSCursor resizeLeftCursor] hotSpot];
NSPoint resizeLeftRight = [[NSCursor resizeLeftRightCursor] hotSpot];
NSPoint resizeRight = [[NSCursor resizeRightCursor] hotSpot];
NSPoint resizeUp = [[NSCursor resizeUpCursor] hotSpot];
NSPoint iBeamCursorForVerticalLayout = [[NSCursor IBeamCursorForVerticalLayout] hotSpot];
*/

long getCursorType() {
   @autoreleasepool {
        return [[[[NSCursor currentSystemCursor] image] TIFFRepresentation] length];
    }
/*
    NSPoint point_of_current_cursor = [[NSCursor currentSystemCursor] hotSpot];

    NSLog(@"1  %lu",(unsigned long)[[[[NSCursor currentSystemCursor] image] TIFFRepresentation] length] );

    NSLog(@"native  %@", NSStringFromPoint(point_of_current_cursor));
    
        if(NSEqualPoints(point_of_current_cursor, arrow))
        {
            return 1;
        }
        else if(NSEqualPoints(point_of_current_cursor, iBeam))
        {
            return 2;
        }
        else if(NSEqualPoints(point_of_current_cursor, resizeUpDown))
        {
            return 3;
        }
        else if(NSEqualPoints(point_of_current_cursor, contextualMenu))
        {
            return 4;
        }
        else if(NSEqualPoints(point_of_current_cursor, pointingHand))
        {
            return 5;
        }
        else if(NSEqualPoints(point_of_current_cursor, closedHand))
        {
            return 6;
        }
        else if(NSEqualPoints(point_of_current_cursor, crosshair))
        {
            return 7;
        }
        else if(NSEqualPoints(point_of_current_cursor, disappearingItem))
        {
            return 8;
        }
        else if(NSEqualPoints(point_of_current_cursor, dragCopy))
        {
            return 9;
        }
        else if(NSEqualPoints(point_of_current_cursor, dragLink))
        {
            return 10;
        }
        else if(NSEqualPoints(point_of_current_cursor, openHand))
        {
            return 11;
        }
        else if(NSEqualPoints(point_of_current_cursor, operationNotAllowed))
        {
            return 12;
         }
        else if(NSEqualPoints(point_of_current_cursor, resizeDown))
        {
            return 13;
        }
        else if(NSEqualPoints(point_of_current_cursor, resizeLeft))
        {
            return 14;
        }
        else if(NSEqualPoints(point_of_current_cursor, resizeLeftRight))
        {
             return 15;
        }
        else if(NSEqualPoints(point_of_current_cursor, resizeRight))
        {
            return 16;
        }
        else if(NSEqualPoints(point_of_current_cursor, resizeUp))
        {
             return 17;
        }
        else if(NSEqualPoints(point_of_current_cursor, iBeamCursorForVerticalLayout))
        {
            return 18;
        }
    return 0;*/
}
