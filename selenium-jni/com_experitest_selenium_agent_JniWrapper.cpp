//
//  com_experitest_selenium_agent_common_macos_jni_MacOsUtils.cpp
//  selenium-jni
//
//  Created by ariel on 07/06/2020.
//  Copyright © 2020 ariel. All rights reserved.
//

#import "com_experitest_selenium_agent_JniWrapper.h"
#import "CursorType-C-Interface.h"

const int JPEG_QUALITY = 50;
const uint32_t windowImage = kCGWindowImageBoundsIgnoreFraming | kCGWindowImageNominalResolution;
const uint32_t windowListOption = kCGWindowListOptionIncludingWindow | kCGWindowListExcludeDesktopElements;

JNIEXPORT jlong JNICALL Java_com_experitest_selenium_agent_JniWrapper_getCursorState
(JNIEnv * env, jclass c)
{
    try {
    //std::cout << get_current_time_and_date() << " getCursorType  " << cc << "\n" << std::flush;;
    return getCursorType();
    } catch (std::exception& e) {
           std::cout << get_current_time_and_date() << " dylib: get exception " << e.what() <<"\n" << std::flush;
    }
    return 0;
}

JNIEXPORT void JNICALL Java_com_experitest_selenium_agent_JniWrapper_cursorView
(JNIEnv * env, jclass c, jboolean show)
{
    try {
        if (show)
        {
            CGDisplayShowCursor(kCGDirectMainDisplay);
        } else {
            CFStringRef propertyString = CFStringCreateWithCString(NULL, "SetsCursorInBackground", kCFStringEncodingMacRoman);
            CGSSetConnectionProperty(_CGSDefaultConnection(), _CGSDefaultConnection(), propertyString, kCFBooleanTrue);
            CFRelease(propertyString);
            CGDisplayHideCursor(kCGDirectMainDisplay);
        }
    } catch (std::exception& e) {
        std::cout << get_current_time_and_date() << " dylib: get exception " << e.what() <<"\n" << std::flush;
    }
}

JNIEXPORT jbyteArray JNICALL Java_com_experitest_selenium_agent_JniWrapper_screenshotCompression
(JNIEnv * env, jclass c, jlong windowId, jboolean isMacAndManual)
{
    try {
        uint32_t wlo = windowListOption;
         CGRect rect = CGRectNull;
        if(isMacAndManual)
        {
             wlo |= kCGWindowListOptionOnScreenAboveWindow;

            uint32_t windowid[1] = {(uint32_t)windowId};
            CFArrayRef windowArray = CFArrayCreate ( NULL, (const void **)windowid, 1 ,NULL);
            CFArrayRef windowsdescription = CGWindowListCreateDescriptionFromArray(windowArray);
            CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(windowsdescription, 0);
            CFDictionaryRef dictRect = (CFDictionaryRef) CFDictionaryGetValue(dict, kCGWindowBounds);
            CGRectMakeWithDictionaryRepresentation(dictRect, &rect);
            CFRelease(windowArray);
            CFRelease(windowsdescription);

//            std::cout << get_current_time_and_date() << "\n"
//            << rect.origin.x << "\n"
//            << rect.origin.y << "\n"
//            << rect.size.height << "\n"
//            << rect.size.width << " \n"
//            << std::flush;
        }
    
        // DESKTOP -> kCGNullWindowID
        CGImageRef cgimage = CGWindowListCreateImage(rect, wlo, (uint32_t)windowId, windowImage);
        
        if (cgimage == NULL) {
            std::cout << get_current_time_and_date() << " dylib: Fail to get image\n" << std::flush;
            return NULL;
        }
        //save_image(cgimage);
        
        size_t bpp = CGImageGetBitsPerPixel(cgimage);
        size_t bpc = CGImageGetBitsPerComponent(cgimage);
        size_t bytes_per_pixel = bpp / bpc;
        
        int width = static_cast<int>(CGImageGetBytesPerRow(cgimage)) / bytes_per_pixel;
        //int width = static_cast<int>(CGImageGetWidth(cgimage)); // given the wrong width
        int height = static_cast<int>(CGImageGetHeight(cgimage));
        //    std::cout << "widthXheight: " << width << " x " << height << "\n" << std::flush;
        
        CFDataRef rawData = CGDataProviderCopyData(CGImageGetDataProvider(cgimage));
        CGImageRelease(cgimage);
        const unsigned char * spc = CFDataGetBytePtr(rawData);// no need to release, only reference
        
        //    int length = static_cast<int>(CFDataGetLength(rawData));
        //    std::cout << "before compress size:" << length << "\n" << std::flush;
        
        if (spc == NULL) {
            CFRelease(rawData);
            std::cout << get_current_time_and_date() << " dylib: Fail to get image byte\n" << std::flush;
            return NULL;
        }
        
        size_t jpegSize = 0;
        unsigned char* compressedImage = NULL; // Memory is allocated by tjCompress2 if _jpegSize == 0
        tjhandle _jpegCompressor = tjInitCompress();
        
        tjCompress2(_jpegCompressor, spc, width, 0, height, TJPF_BGRA,
                    &compressedImage, &jpegSize, TJSAMP_420, JPEG_QUALITY, TJFLAG_FASTDCT);
        
        CFRelease(rawData);
        tjDestroy(_jpegCompressor);
        
        if (compressedImage == NULL) {
            std::cout << get_current_time_and_date() << " dylib: Fail to compress image\n" << std::flush;
            tjFree(compressedImage);
            return NULL;
        }
        
        //    std::cout << "after compress size:" << jpegSize << "\n" << std::flush;
        
        jbyteArray returnArray = env->NewByteArray((int)jpegSize);
        env->SetByteArrayRegion(returnArray, 0, (int)jpegSize, (jbyte*) compressedImage);
        
        //env->ReleaseByteArrayElements(returnArray, (jbyte*)compressedImage, NULL);
        
        tjFree(compressedImage);
        
        //    FILE *stream = fopen("/Users/ariel/Desktop/afterJurboJPEG.png", "w+");
        //    fwrite(_compressedImage, bpp, width * height, stream);
        
        return returnArray;
    } catch (std::exception& e) {
        std::cout << get_current_time_and_date() << " dylib: get exception " << e.what() <<"\n" << std::flush;
    }
    return NULL;
}

int main(int ac, char** av)
{
    while (true){
    std::cout << get_current_time_and_date() << " main output ----" <<"\n" << std::flush;
      //  Java_com_experitest_selenium_agent_JniWrapper_cursorView(NULL, NULL, true);
      //  Java_com_experitest_selenium_agent_JniWrapper_cursorView(NULL, NULL, false);
        Java_com_experitest_selenium_agent_JniWrapper_getCursorState(NULL, NULL);
    //Java_com_experitest_selenium_agent_JniWrapper_screenshotCompression(NULL,NULL,968,true);
    }
    return 0;
}

void print_CGImageRef_info(CGImage* cgimage)
{
    size_t width = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
     size_t bpr = CGImageGetBytesPerRow(cgimage);
     size_t bpp = CGImageGetBitsPerPixel(cgimage);
     size_t bpc = CGImageGetBitsPerComponent(cgimage);
     size_t bytes_per_pixel = bpp / bpc;

     CGBitmapInfo info = CGImageGetBitmapInfo(cgimage);

        std::cout << width                                                            << "\n" << std::flush;
        std::cout << height                                                           << "\n" << std::flush;
        std::cout << CGImageGetColorSpace(cgimage)                                    << "\n" << std::flush;
        std::cout << bpr                                                              << "\n" << std::flush;
        std::cout << bpp                                                              << "\n" << std::flush;
        std::cout << bpc                                                              << "\n" << std::flush;
        std::cout << bytes_per_pixel                                                  << "\n" << std::flush;
        std::cout << width                                                            << "\n" << std::flush;
        std::cout << info                                                             << "\n" << std::flush;
        std::cout << (info & kCGBitmapAlphaInfoMask)                                  << "\n" << std::flush;
        std::cout << (info & kCGBitmapFloatComponents)                                << "\n" << std::flush;
        std::cout << (info & kCGBitmapByteOrderMask)                                  << "\n" << std::flush;
        std::cout << ((info & kCGBitmapByteOrderMask) == kCGBitmapByteOrderDefault)   << "\n" << std::flush;
        std::cout << ((info & kCGBitmapByteOrderMask) == kCGBitmapByteOrder16Little)  << "\n" << std::flush;
        std::cout << ((info & kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Little)  << "\n" << std::flush;
        std::cout << ((info & kCGBitmapByteOrderMask) == kCGBitmapByteOrder16Big)     << "\n" << std::flush;
        std::cout << ((info & kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Big)     << "\n" << std::flush;
}


std::string get_current_time_and_date()
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
    return ss.str();
}

void save_image(CGImage* cgimage)
{
    CFStringRef file = CFSTR("/Users/Shared/SeleniumAgent/CGImage.jpg");
    CFStringRef type = CFSTR("public.jpeg");
    CFURLRef urlRef = CFURLCreateWithFileSystemPath( kCFAllocatorDefault, file, kCFURLPOSIXPathStyle, false );
    CGImageDestinationRef idst = CGImageDestinationCreateWithURL( urlRef, type, 1, NULL );
    CGImageDestinationAddImage( idst, cgimage, NULL );
    CGImageDestinationFinalize( idst );
}

JNIEXPORT void JNICALL Java_com_experitest_selenium_agent_JniWrapper_init
(JNIEnv *, jclass)
{

}

JNIEXPORT jbyteArray JNICALL Java_com_experitest_selenium_agent_JniWrapper_screenCaptureWithWindowId
(JNIEnv *, jclass, jlong)
{
    std::cout << " -----------------1" << std::flush;
    CGImageRef image_ref = CGDisplayCreateImage(CGMainDisplayID());
    CGDataProviderRef provider = CGImageGetDataProvider(image_ref);
    CFDataRef dataref = CGDataProviderCopyData(provider);
    size_t width, height;
    std::cout << " -----------------2" << std::flush;
    width = CGImageGetWidth(image_ref);
    height = CGImageGetHeight(image_ref);
    size_t bpp = CGImageGetBitsPerPixel(image_ref) / 8;
    std::cout << " -----------------3" << width << " " << height << std::flush;

    unsigned char *pixels = (unsigned char*) malloc(width * height);
    memcpy(pixels, CFDataGetBytePtr(dataref), width * height * bpp);
    CFRelease(dataref);
    std::cout << " -----------------4" << std::flush;

    CGImageRelease(image_ref);
    FILE *stream = fopen("/Users/ariel/Desktop/screencap.png", "w+");
    fwrite(pixels, bpp, width * height, stream);
    fclose(stream);
    free(pixels);
    std::cout << " -----------------5" << std::flush;
    return NULL;
}
