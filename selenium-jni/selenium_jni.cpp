//
//  selenium_jni.cpp
//  selenium-jni
//
//  Created by ariel on 07/06/2020.
//  Copyright © 2020 ariel. All rights reserved.
//

#include <iostream>
#include "selenium_jni.hpp"
#include "selenium_jniPriv.hpp"

void selenium_jni::HelloWorld(const char * s)
{
    selenium_jniPriv *theObj = new selenium_jniPriv;
    theObj->HelloWorldPriv(s);
    delete theObj;
};

void selenium_jniPriv::HelloWorldPriv(const char * s) 
{
    std::cout << s << std::endl;
};

