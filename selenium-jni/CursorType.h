//
//  CursorType-C-Interface.h
//  selenium-jni
//
//  Created by ariel on 02/09/2020.
//  Copyright © 2020 ariel. All rights reserved.
//
#import "CursorType-C-Interface.h"
#import <Cocoa/Cocoa.h>

// An Objective-C class that needs to be accessed from C++
@interface CursorType : NSObject

// The Objective-C member function you want to call from C++
- (long) getCursorType;
@end
