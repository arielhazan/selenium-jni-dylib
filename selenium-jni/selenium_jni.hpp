//
//  selenium_jni.hpp
//  selenium-jni
//
//  Created by ariel on 07/06/2020.
//  Copyright © 2020 ariel. All rights reserved.
//

#ifndef selenium_jni_
#define selenium_jni_

/* The classes below are exported */
#pragma GCC visibility push(default)

class selenium_jni
{
    public:
    void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
