//
//  selenium_jniPriv.hpp
//  selenium-jni
//
//  Created by ariel on 07/06/2020.
//  Copyright © 2020 ariel. All rights reserved.
//

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class selenium_jniPriv
{
    public:
    void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
