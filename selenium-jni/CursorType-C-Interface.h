//
//  CursorType-C-Interface.h
//  selenium-jni
//
//  Created by ariel on 02/09/2020.
//  Copyright © 2020 ariel. All rights reserved.
//
#ifndef __CURSORTYPE_C_INTERFACE_H__
#define __CURSORTYPE_C_INTERFACE_H__

// This is the C "trampoline" function that will be used
// to invoke a specific Objective-C method FROM C++
long getCursorType();

#endif
